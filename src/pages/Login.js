import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import {useState, useEffect} from 'react'


export default function Login(){

	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(() => {
		if(email != "" && pw != ""){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	}, [email, pw])

	const loginUser = (e) =>{
		e.preventDefault()
		alert(`Successfuly Login`)
	}

	return(
		<Container className="m-5">
			<h1 className="text-center">Login</h1>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => loginUser(e)}>
					  
					  <Form.Group className="mb-3">
						    <Form.Label>Email address</Form.Label>
						    <Form.Control 
						    type="text" 
						    value={email}
						    onChange={(e) => {
                                setEmail(e.target.value)}}
						    />
					  </Form.Group>

					  <Form.Group className="mb-3">
						    <Form.Label>Password</Form.Label>
						    <Form.Control 
						    type="password" 
						    value={pw}
						    onChange={(e) => {
                               setPW(e.target.value)}}
						    />
					  </Form.Group>

					  <Button 
					  variant="info" 
					  type="submit"
					  disabled={isDisabled}
					  >
					  Login</Button>
					</Form>
				</Col>
			</Row>		
		</Container>
	)
}	