import {Fragment} from 'react'
import AppNavbar from './../components/AppNavbar'
import Banner from './../components/Banner'
import Footer from './../components/Footer'
import Highlights from './../components/Highlights'


export default function Home(){
	return (
		// render navbar, banner & footer in the webpage via home.js
	<Fragment>
		<AppNavbar/>
		<Banner/>
		<Highlights/>
		<Footer/>
	</Fragment>
	)
}