import coursesData from './../mockData/courses'
import CourseCard from './../components/CourseCard'
import {Fragment} from 'react';


export default function Courses(){
	const courses = coursesData.map(course => {
			return <CourseCard key={course.id} courseProp={course} />
	})

	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}