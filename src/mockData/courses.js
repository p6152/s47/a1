

let coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem, ipsum dolor, sit amet consectetur adipisicing elit. Pariatur reiciendis maiores odit sed commodi dolore. Illum, magnam, dolorum. Ea unde, praesentium molestias minus suscipit recusandae, totam nesciunt ipsam consectetur vitae.",
		price: 25000,
		onOffer: true
	},

	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem, ipsum dolor, sit amet consectetur adipisicing elit. Pariatur reiciendis maiores odit sed commodi dolore. Illum, magnam, dolorum. Ea unde, praesentium molestias minus suscipit recusandae, totam nesciunt ipsam consectetur vitae.",
		price: 35000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem, ipsum dolor, sit amet consectetur adipisicing elit. Pariatur reiciendis maiores odit sed commodi dolore. Illum, magnam, dolorum. Ea unde, praesentium molestias minus suscipit recusandae, totam nesciunt ipsam consectetur vitae.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "NodeJS-ExpressJS",
		description: "Lorem, ipsum dolor, sit amet consectetur adipisicing elit. Pariatur reiciendis maiores odit sed commodi dolore. Illum, magnam, dolorum. Ea unde, praesentium molestias minus suscipit recusandae, totam nesciunt ipsam consectetur vitae.",
		price: 55000,
		onOffer: false
	}
]

export default coursesData