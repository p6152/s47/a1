import {Navbar, Container, Nav} from 'react-bootstrap'


export default function AppNavbar(){
	return (
	  <Navbar bg="info" expand="lg">
	      <Container>
	        <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">
	            <Nav.Link href="/">Home</Nav.Link>
	            <Nav.Link href="/courses">Courses</Nav.Link> 
	            <Nav.Link href="/login">Login</Nav.Link>     
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	  </Navbar>
	)
}

