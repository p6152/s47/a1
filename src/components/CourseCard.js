import {useState, useEffect} from 'react'
import {Card, Row, Col, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}){

	const [count, setCount] = useState(0)

	const [seat, setSeat] = useState(30)

	const {name, description, price} = courseProp
	
	// useEffect(() => console.log(`render`), [count])

	const handleClick = () => {
		
		if(seat <= 30 && seat > 0){
			setSeat(seat - 1)
			setCount(count + 1)
		}else{
			alert(`No more seats`)
		}
	}

	return(
	<Card className="m-5">
	  <Card.Body>
	    <Card.Title>{name}</Card.Title>
	    <Card.Subtitle className="mb-2 text-muted">Description</Card.Subtitle>
	    <Card.Text>
	      {description}
	    </Card.Text>
	    <Card.Subtitle className="mb-2 text-muted">Price</Card.Subtitle>
	    <Card.Text>
	      {price}
	    </Card.Text>
	    {/*<Card.Text>Count: {count}</Card.Text>*/}
	    <Card.Subtitle className="mb-2 text-muted">Enrollees</Card.Subtitle>
	    <Card.Text>{count} Enrollees</Card.Text>
	    <a className="btn btn-outline-primary" href="#" onClick={handleClick}>Enroll</a>
	  </Card.Body>
	</Card>
	)
}